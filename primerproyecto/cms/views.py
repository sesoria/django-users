from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout

# Create your views here.

FORM = """
    <hr>
    <form action="" method="post">
      <div>
        <label>Valor del recurso: </label>
        <input type="text" name="valor" required>
      </div>
      <div>
        <input type="submit" name = "action" value="Enviar ">
      </div>
"""

def index(request):
    # obtener el contendio del template
    content_list = Contenido.objects.all()
    # cargar el template
    template = loader.get_template("cms/index.html")
    # crear el contexto
    contexto = {
        "contenido_lista": content_list
    }
    # Renderizar el template y responder
    return HttpResponse(template.render(contexto, request))


@csrf_exempt
def get_resources(request, recurso):
    if request.method == "PUT" and request.user.is_authenticated:
        val = request.body.decode()
        try:
            contenido = Contenido.objects.get(clave=recurso)
            contenido.valor = val
            contenido.save()
        except Contenido.DoesNotExist:
            contenido = Contenido(clave=recurso, valor=val)
            contenido.save()

    if request.method == "POST":
        val = request.POST['valor']
        try:
            contenido = Contenido.objects.get(clave=recurso)
            contenido.valor = val
            contenido.save()

        except Contenido.DoesNotExist:
            c = Contenido(clave=recurso, valor=val)
            c.save()
    # Metodos get
    try:
        contenido = Contenido.objects.get(clave=recurso)
        html = '<html><body>' \
               f'<div>El contenido del recurso "{contenido.clave}" es : {contenido.valor}</div>'
        html += f'<div>{FORM if request.user.is_authenticated else "Para modificarlo entra en <a href=/login>login</a>"}</div>' \
                '</body></html>'
    except Contenido.DoesNotExist:
        html = '<html><body>' \
               f'<div>El recurso pedido no es existe, pero puedes crearlo!</div>' \
               f'<div>{FORM if request.user.is_authenticated else "No estás logeado, para crearlo entra en <a href=/login>login</a>"}</div>' \
               '</body></html>'
    return HttpResponse(html)


def loggedIn(request):
    if request.user.is_authenticated:
        respuesta = f"El usuario {request.user.username} está autenticado"
    else:
        respuesta = "No estás logeado, entra en el <a href=/login>login</a>"
    return HttpResponse(respuesta)


def logout_vista(request):
    logout(request)
    return redirect("/cms/")


def image(request):
    template = loader.get_template("cms/plantilla.html")
    contexto = {}
    return HttpResponse(template.render(contexto, request))
