from django.urls import path
from . import views

urlpatterns = [
    # path('', views.index),
    path('', views.index),
    path('login', views.loggedIn),
    path('logout', views.logout_vista),
    path('image', views.image),
    path('<str:recurso>/', views.get_resources),

]