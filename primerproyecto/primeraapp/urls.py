from django.urls import path
from . import views

urlpatterns = [
	path('', views.index),
	path('hola/', views.di_hola),
	path('adios/<nombre>/', views.di_adios),
	path('adios/<int:num1>/<int:num2>/', views.di_numeros),
]
